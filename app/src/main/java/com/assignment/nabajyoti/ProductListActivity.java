package com.assignment.nabajyoti;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.assignment.nabajyoti.adapter.ProductsAdapter;
import com.assignment.nabajyoti.model.Product;
import com.orm.SugarRecord;

import java.util.List;

public class ProductListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    private ProductsAdapter productsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productlist_activity_main);
        //Set Action Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Set up recycler view
        recyclerView = findViewById(R.id.recyclerViewProductList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        //Get sample product list
        getSampleProducts();
        if (productsAdapter != null) {
            recyclerView.setAdapter(productsAdapter);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            List<Product> productList = SugarRecord.listAll(Product.class, "PRICE ASC");
            productsAdapter = new ProductsAdapter(productList);
            recyclerView.setAdapter(productsAdapter);
            Toast.makeText(this, "Sorted by Price : Low to High", Toast.LENGTH_LONG).show();
            return true;
        } else if (id == R.id.action_cart) {
            openShoppingCart();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openShoppingCart() {
        startActivity(new Intent(this, ShoppingCartActivity.class));
    }

    private void getSampleProducts() {
        List<Product> productList = SugarRecord.listAll(Product.class);
        if (productList.size() == 0) {
            Product product1 = new Product("Product 1", 1200, false);
            product1.save();
            productList.add(product1);
            Product product2 = new Product("Product 2", 2200, false);
            product2.save();
            productList.add(product2);
            Product product3 = new Product("Product 3", 1001.55, false);
            product3.save();
            productList.add(product3);
            Product product4 = new Product("Product 4", 1100.52, false);
            product4.save();
            productList.add(product4);
            Product product5 = new Product("Product 5", 10, false);
            product5.save();
            productList.add(product5);
            Product product6 = new Product("Product 6", 250, false);
            product6.save();
            productList.add(product6);
            Product product7 = new Product("Product 7", 135.48, false);
            product7.save();
            productList.add(product7);
            Product product8 = new Product("Product 8", 530, false);
            product8.save();
            productList.add(product8);
            Product product9 = new Product("Product 9", 900, false);
            product9.save();
            productList.add(product9);
            Product product10 = new Product("Product 10", 800, false);
            product10.save();
            productList.add(product10);
        }
        productsAdapter = new ProductsAdapter(productList);
    }
}
