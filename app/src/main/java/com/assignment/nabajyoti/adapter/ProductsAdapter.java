package com.assignment.nabajyoti.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.assignment.nabajyoti.R;
import com.assignment.nabajyoti.model.Cart;
import com.assignment.nabajyoti.model.Product;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created on 30-05-2019.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> productList;

    public ProductsAdapter(List<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final Product product = productList.get(i);
        viewHolder.titleTextView.setText(product.getTitle());
        viewHolder.priceTextView.setText("\u20B9 " + new DecimalFormat("##.##").format(product.getPrice()));
        if (product.isAddedToCart()) {
            viewHolder.buttonAddToCart.setText(R.string.remove_cart);
        } else {
            viewHolder.buttonAddToCart.setText(R.string.add_to_cart);
        }
        viewHolder.buttonAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!product.isAddedToCart()) {
                    Cart cart = new Cart();
                    cart.setProductId(product.getId());
                    if (cart.save() > 0) {
                        Toast.makeText(view.getContext(), "Item added to cart", Toast.LENGTH_SHORT).show();
                        product.setAddedToCart(true);
                        product.update();
                    }
                } else {
                    Cart.deleteAll(Cart.class, "PRODUCT_ID ='" + product.getId() + "'");
                    product.setAddedToCart(false);
                    product.update();
                    Toast.makeText(view.getContext(), "Removed from Cart", Toast.LENGTH_SHORT).show();
                }
                notifyItemChanged(viewHolder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleTextView;
        TextView priceTextView;
        Button buttonAddToCart;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.textViewTitle);
            priceTextView = itemView.findViewById(R.id.textViewPrice);
            buttonAddToCart = itemView.findViewById(R.id.buttonAddToCart);

        }
    }
}
