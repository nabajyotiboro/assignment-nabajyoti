package com.assignment.nabajyoti.model;

import com.orm.SugarRecord;

/**
 * Created on 29-05-2019.
 */
public class Cart extends SugarRecord {

    private long productId;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
