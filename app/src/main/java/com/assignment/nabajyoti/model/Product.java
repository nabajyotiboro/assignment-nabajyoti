package com.assignment.nabajyoti.model;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

/**
 * Created on 29-05-2019.
 */
public class Product extends SugarRecord {

    @Unique
    private long prodId;
    private String title;
    private double price;
    private boolean isAddedToCart;

    public Product(String title, double price, boolean isAddedToCart) {
        this.title = title;
        this.price = price;
        this.isAddedToCart = isAddedToCart;
        prodId = Math.round(Math.random() * 1000);
    }

    public Product() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAddedToCart() {
        return isAddedToCart;
    }

    public void setAddedToCart(boolean addedToCart) {
        isAddedToCart = addedToCart;
    }

    public long getProdId() {
        return prodId;
    }

    public void setProdId(long prodId) {
        this.prodId = prodId;
    }
}
