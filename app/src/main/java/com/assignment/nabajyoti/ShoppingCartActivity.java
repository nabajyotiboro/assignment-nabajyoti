package com.assignment.nabajyoti;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.assignment.nabajyoti.model.Cart;
import com.assignment.nabajyoti.model.Product;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

/**
 * Created on 31-05-2019.
 */
public class ShoppingCartActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Cart> cartList;
    TextView totalBillTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_cart_activity_main);
        //Set Action Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        //Initialise views
        totalBillTextView = findViewById(R.id.textViewTotalBill);
        recyclerView = findViewById(R.id.recyclerViewCartList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(layoutManager);

        //Get list of added items in Cart
        cartList = Cart.listAll(Cart.class);
        if (cartList.size() == 0) {
            findViewById(R.id.noItemsTextView).setVisibility(View.VISIBLE);
            findViewById(R.id.checkOutLayout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.noItemsTextView).setVisibility(View.GONE);
            findViewById(R.id.checkOutLayout).setVisibility(View.VISIBLE);
            CartAdapter adapter = new CartAdapter();
            recyclerView.setAdapter(adapter);

            //Calculate Total Bill amount
            updateTotalBillAmount();
        }
    }

    private void updateTotalBillAmount() {
        double total = 0;
        for (Cart cart : cartList) {
            Product product = Cart.findById(Product.class, cart.getProductId());
            total = total + product.getPrice();
        }

        totalBillTextView.setText(String.format(getString(R.string.total_bill_amount), total));
    }

    class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {
        @NonNull
        @Override
        public CartHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new CartHolder(getLayoutInflater().inflate(R.layout.item_cart, viewGroup, false));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull final CartHolder cartHolder, int i) {
            final Cart itemCart = cartList.get(cartHolder.getAdapterPosition());
            final Product product = Cart.findById(Product.class, itemCart.getProductId());
            cartHolder.titleTextView.setText(product.getTitle());
            cartHolder.priceTextView.setText("\u20B9 " + new DecimalFormat("##.##").format(product.getPrice()));
            cartHolder.buttonRemoveCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartList.remove(itemCart);
                    Cart.delete(itemCart);
                    product.setAddedToCart(false);
                    product.update();
                    notifyItemRemoved(cartHolder.getAdapterPosition());
                    //Calculate Total Bill amount
                    updateTotalBillAmount();
                    if (cartList.size() == 0) {
                        findViewById(R.id.noItemsTextView).setVisibility(View.VISIBLE);
                        findViewById(R.id.checkOutLayout).setVisibility(View.GONE);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return cartList.size();
        }

        class CartHolder extends RecyclerView.ViewHolder {
            TextView titleTextView;
            TextView priceTextView;
            Button buttonRemoveCart;

            CartHolder(@NonNull View itemView) {
                super(itemView);
                titleTextView = itemView.findViewById(R.id.textViewTitle);
                priceTextView = itemView.findViewById(R.id.textViewPrice);
                buttonRemoveCart = itemView.findViewById(R.id.buttonRemoveCart);
            }
        }
    }
}
